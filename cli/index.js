const inquirer = require('inquirer');
const axios = require('axios');
const Table = require('cli-table');

const config = {
  host: 'http://127.0.0.1',
  timeout: 200,
  port: 3000,
  headers: { 'X-Application-Id': 'cabify-basket-cli' },
};

const axiosInstance = axios.create({
  baseURL: `${config.host}:${config.port}`,
  timeout: config.timeout,
  headers: config.headers,
});

const print = products => {
  const table = new Table({
    head: ['Codigo', 'Nombre', 'Precio'],
  });
  products.forEach(p => table.push([p.code, p.name, p.price]));
  console.log(table.toString());
};

const printBasket = async basket => {
  console.log('Tu carrito :');
  const products = await getAllProductsInBasket(basket);
  print(products);
};

const transform = products =>
  products.map(p => ({
    name: p.name,
    value: p.code,
  }));

// Using promises just to show i can use both
const getAllProducts = () =>
  axiosInstance.get('/products').then(res => res.data);
const createBasket = () =>
  axiosInstance.post('/basket').then(res => res.data.id);
const getAllProductsInBasket = async basketId =>
  axiosInstance.get(`/basket/${basketId}/products`).then(res => res.data);
const addProduct = async (basketId, product) =>
  axiosInstance.post(`/basket/${basketId}/product`, {
    id: product,
  });
const removeProduct = async (basketId, product) =>
  axiosInstance.delete(`basket/${basketId}/products/${product}`);
const getTotals = async basketId =>
  axiosInstance.get(`basket/${basketId}/total`).then(res => res.data);

const init = async () => {
  console.log('Bienvenido al Cabify SHOP');
  const products = await getAllProducts();
  print(products);
  const productsInquirer = transform(products);
  const basketId = await createBasket();
  await askAdd(basketId, productsInquirer);
  askAction(basketId, productsInquirer);
};

const askAdd = async (basketId, products) => {
  const answer = await inquirer.prompt({
    type: 'list',
    name: 'product',
    message: 'Qué vas a elegir?',
    choices: products,
  });
  const { product } = answer;
  try {
    await addProduct(basketId, product);
    await printBasket(basketId);
  } catch (e) {
    console.log(e);
  }
};

const askRemove = async basketId => {
  const products = await getAllProductsInBasket(basketId);
  const productsInquirer = transform(products);
  const answer = await inquirer.prompt({
    type: 'list',
    name: 'product',
    message: 'Qué vas a elegir?',
    choices: productsInquirer,
  });
  const { product } = answer;
  try {
    await removeProduct(basketId, product);
    await printBasket(basketId);
  } catch (e) {
    console.log(e);
  }
};

const checkout = async basketId => {
  try {
    const { gross, total, savings } = await getTotals(basketId);
    console.log('Terminando su compra ...');
    console.log('El total sin discuentos es: ', gross);
    console.log('Applying discounts ...');
    console.log('TOTAL : ', total);
    console.log('Con tu compra ahorraste ', savings);
  } catch (e) {
    console.log(e);
  }
};
const askAction = async (basketId, products) => {
  const actions = [
    {
      name: 'Agregar otro producto al carrito',
      value: 'add',
    },
    {
      name: 'Eliminar un producto del carrito',
      value: 'remove',
    },
    {
      name: 'Terminar',
      value: 'checkout',
    },
  ];
  const answer = await inquirer.prompt({
    type: 'list',
    name: 'action',
    message: 'Qué desea hacer?',
    choices: actions,
  });
  switch (answer.action) {
    case 'add':
      await askAdd(basketId, products);
      askAction(basketId, products);
      break;
    case 'remove':
      await askRemove(basketId);
      askAction(basketId, products);
      break;
    case 'checkout':
      checkout(basketId);
      break;
  }
};

init();
