package model

import (
	"testing"
)

func TestCalculateFreeProduct(t *testing.T) {
	fp := FreeProduct{Code: "VOUCHER"}
	testBasket := &Basket{[]*Product{
		&Product{Code: "VOUCHER", Name: "Cabify Voucher", Price: 5.00},
		&Product{Code: "TSHIRT", Name: "Cabify T-Shirt", Price: 20.00},
		&Product{Code: "VOUCHER", Name: "Cabify Voucher", Price: 5.00},
	}}
	expected := 25.0
	fp.CalculateAmmount(testBasket)
	given := testBasket.GetDiscountAmmount()
	if given != expected {
		t.Errorf("Got: %f, expected: %f", given, expected)
	}

}

func TestCalculateBulkProduct(t *testing.T) {
	bd := BulkDiscount{Code: "TSHIRT", Quantity: 3, NewPrice: 19}
	testBasket := &Basket{[]*Product{
		&Product{Code: "TSHIRT", Name: "Cabify T-Shirt", Price: 20.00},
		&Product{Code: "TSHIRT", Name: "Cabify T-Shirt", Price: 20.00},
		&Product{Code: "TSHIRT", Name: "Cabify T-Shirt", Price: 20.00},
		&Product{Code: "VOUCHER", Name: "Cabify Voucher", Price: 5.00},
		&Product{Code: "TSHIRT", Name: "Cabify T-Shirt", Price: 20.00},
	}}

	expected := 81.0
	bd.CalculateAmmount(testBasket)
	given := testBasket.GetDiscountAmmount()
	if given != expected {
		t.Errorf("Got: %f, expected: %f", given, expected)
	}
}

func TestMix(t *testing.T) {
	bd := BulkDiscount{Code: "TSHIRT", Quantity: 3, NewPrice: 19}
	fp := FreeProduct{Code: "VOUCHER"}
	testBasket := &Basket{[]*Product{
		&Product{Code: "VOUCHER", Name: "Cabify Voucher", Price: 5.00},
		&Product{Code: "TSHIRT", Name: "Cabify T-Shirt", Price: 20.00},
		&Product{Code: "VOUCHER", Name: "Cabify Voucher", Price: 5.00},
		&Product{Code: "VOUCHER", Name: "Cabify Voucher", Price: 5.00},
		&Product{Code: "MUG", Name: "Cabify Coffee Mug", Price: 7.50},
		&Product{Code: "TSHIRT", Name: "Cabify T-Shirt", Price: 20.00},
		&Product{Code: "TSHIRT", Name: "Cabify T-Shirt", Price: 20.00},
	}}

	expected := 74.50
	fp.CalculateAmmount(testBasket)
	bd.CalculateAmmount(testBasket)
	given := testBasket.GetDiscountAmmount()
	if given != expected {
		t.Errorf("Got: %f, expected: %f", given, expected)
	}
}
