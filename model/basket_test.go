package model

import (
	"testing"
)

var (
	p1 = Product{Code: "VOUCHER", Name: "Cabify Voucher", Price: 5.00}
	p2 = Product{Code: "TSHIRT", Name: "Cabify T-Shirt", Price: 20.00}
	p3 = Product{Code: "MUG", Name: "Cabify Coffee Mug", Price: 7.50}
)

func TestAddProduct(t *testing.T) {
	b := &Basket{}
	if len(b.products) != 0 {
		t.Errorf("Basket product length should be 0, but is %d", len(b.products))
	}

	b.AddProduct(&p1)
	if len(b.products) != 1 {
		t.Errorf("Basket product length should be 1, but is %d", len(b.products))
	}
}

func TestRemoveProduct(t *testing.T) {
	b := &Basket{[]*Product{&p1}}
	if len(b.products) != 1 {
		t.Errorf("Basket product length should be 0, but is %d", len(b.products))
	}

	b.RemoveProduct(&p1)
	if len(b.products) != 0 {
		t.Errorf("Basket product length should be 0, but is %d", len(b.products))
	}
}

func TestGetAllProducts(t *testing.T) {
	b := &Basket{[]*Product{&p1, &p2, &p3}}
	products := b.GetAllProducts()

	if len(products) != 3 {
		t.Errorf("Basket product length should be 3, but is %d", len(b.products))
	}
}
