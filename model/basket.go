// Package basket provides a representation for a online eshop basket
package model

type Basket struct {
	products []*Product
}

type BasketProduct struct {
	product  *Product
	quantity int
}

type BasketHelper struct {
	products map[int]*BasketProduct
}

func NewBasket() *Basket {
	return &Basket{}
}

func (b *Basket) AddProduct(p *Product) {
	b.products = append(b.products, p)
}

func (b *Basket) RemoveProduct(p *Product) {
	for i, basketProduct := range b.products {
		if basketProduct.Code == p.Code {
			b.products = append(b.products[:i], b.products[i+1:]...)
			break
		}
	}
}

func (b *Basket) GetAllProducts() []*Product {
	return b.products
}

func (b *Basket) GetDiscountAmmount() float64 {
	var total float64
	for _, p := range b.products {
		total += p.Price * (1 - p.Discount)
	}
	return total
}

func (b *Basket) GetTotalAmmount() float64 {
	var total float64
	for _, p := range b.products {
		total += p.Price
	}
	return total
}

func (b *Basket) RefreshDiscount() {
	for _, p := range b.products {
		p.Discount = 0
	}
}
