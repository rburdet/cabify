package model

// Product is a product representation of a cabify product
type Product struct {
	Code     string  `json:"code"`
	Name     string  `json:"name"`
	Price    float64 `json:"price"`
	Discount float64 `json:"-"`
}
