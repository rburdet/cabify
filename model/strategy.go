// Package promotion declares cabify promotion strategies
package model

type DiscountCalculator interface {
	CalculateAmmount(b *Basket)
}

// freeProduct specifies promotions of the type 2x1
type FreeProduct struct {
	Code string
}

type BulkDiscount struct {
	Code     string
	Quantity int
	NewPrice float64
}

func (fp *FreeProduct) CalculateAmmount(b *Basket) {
	var discountCandidate *Product
	productCount := 0
	for _, p := range b.products {
		if fp.Code == p.Code {
			productCount += 1
			if productCount%2 == 0 {
				p.Discount = p.Discount + 0.5
				discountCandidate.Discount = discountCandidate.Discount + 0.5
			} else {
				discountCandidate = p
			}
		}
	}

}

func (bd *BulkDiscount) CalculateAmmount(b *Basket) {
	var targetedProducts []*Product
	targetCounter := 0
	for _, p := range b.products {
		if bd.Code == p.Code {
			targetCounter += 1
			targetedProducts = append(targetedProducts, p)
		}
	}

	if targetCounter >= bd.Quantity {
		for _, p := range targetedProducts {
			discount := (p.Price - bd.NewPrice) / p.Price
			p.Discount = p.Discount + discount
		}
	}
}
