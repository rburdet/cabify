package db

import (
	"gitlab.com/rburdet/cabify/model"
)

type MemDb interface {
	SaveBasket(id string, b *model.Basket)
	GetBasket(id string) *model.Basket
	DeleteBasket(id string)
}

type MemDbImpl struct {
	data map[string]*model.Basket
}

func NewMemDbImpl() *MemDbImpl {
	return &MemDbImpl{make(map[string]*model.Basket)}
}

func (m *MemDbImpl) SaveBasket(id string, b *model.Basket) {
	m.data[id] = b
}

func (m *MemDbImpl) GetBasket(id string) *model.Basket {
	return m.data[id]
}

func (m *MemDbImpl) DeleteBasket(id string) {
	delete(m.data, id)
}
