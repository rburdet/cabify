// Package db provides products database functionality
package db

import (
	"gitlab.com/rburdet/cabify/model"
)

type Db interface {
	GetProduct(id string) *model.Product
	GetAllProducts() []*model.Product
}

// DbImpl is a memory mocked abstraction of a database, real case scenarios
// should provide an api for external databases
type DbImpl struct {
	data map[string]*model.Product
}

type PromotionDb struct {
	strategies []model.DiscountCalculator
}

func NewPromotionsDb() PromotionDb {
	bd := &model.BulkDiscount{Code: "TSHIRT", Quantity: 3, NewPrice: 19}
	fp := &model.FreeProduct{Code: "VOUCHER"}

	return PromotionDb{[]model.DiscountCalculator{fp, bd}}
}

var (
	p1 = &model.Product{Code: "VOUCHER", Name: "Cabify Voucher", Price: 5.00}
	p2 = &model.Product{Code: "TSHIRT", Name: "Cabify T-Shirt", Price: 20.00}
	p3 = &model.Product{Code: "MUG", Name: "Cabify Coffee Mug", Price: 7.50}
)

// NewDbImpl returns a new *DbImpl instance
func NewDbImpl() *DbImpl {
	return &DbImpl{
		data: map[string]*model.Product{
			"VOUCHER": p1,
			"TSHIRT":  p2,
			"MUG":     p3,
		},
	}
}

func (db *DbImpl) GetProduct(code string) *model.Product {
	p := db.data[code]
	return &model.Product{
		Code:  p.Code,
		Name:  p.Name,
		Price: p.Price,
	}
}

// GetAllProducts return all the products present in the db
func (db *DbImpl) GetAllProducts() []*model.Product {
	var products []*model.Product
	products = append(products, db.data["VOUCHER"])
	products = append(products, db.data["TSHIRT"])
	products = append(products, db.data["MUG"])

	return products
}

func (pdb *PromotionDb) GetStrategies() []model.DiscountCalculator {
	return pdb.strategies
}
