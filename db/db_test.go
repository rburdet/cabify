package db

import (
	"gitlab.com/rburdet/cabify/model"
	"testing"
)

func TestGetAllProducts(t *testing.T) {
	expected := []*model.Product{
		&model.Product{Code: "VOUCHER", Name: "Cabify Voucher", Price: 5.00},
		&model.Product{Code: "TSHIRT", Name: "Cabify T-Shirt", Price: 20.00},
		&model.Product{Code: "MUG", Name: "Cabify Coffee Mug", Price: 7.50},
	}

	given := NewDbImpl().GetAllProducts()

	for i := 0; i < 3; i++ {
		if *given[i] != *expected[i] {
			t.Errorf("Got %v, expected %v", given[i], expected[i])
		}
	}
}
