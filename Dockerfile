FROM golang:alpine as builder

WORKDIR /go/src/gitlab.com/rburdet/cabify
RUN apk add --no-cache make build-base git
COPY . .
RUN make

FROM alpine
COPY --from=builder /go/src/gitlab.com/rburdet/cabify/cabify .
CMD ["./cabify"]

