package main

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"gitlab.com/rburdet/cabify/db"
	"gitlab.com/rburdet/cabify/handler"
	"gitlab.com/rburdet/cabify/service"
)

const (
	port = ":3000"
)

func main() {
	e := echo.New()
	e.Use(middleware.Logger())

	loadHealthRoute(e)
	loadProductHandlerRoute(e)

	g := e.Group("/basket")
	loadBasketRoute(g)

	e.Logger.Fatal(e.Start(port))
}

func loadHealthRoute(e *echo.Echo) {
	hh := &handler.HealthHandler{}
	e.GET("/health", hh.Health)
}

func loadProductHandlerRoute(e *echo.Echo) {
	prodDb := db.NewDbImpl()
	ps := service.NewProductService(prodDb)
	ph := handler.NewProductHandler(ps)
	e.GET("/products", ph.ListAllProducts)
}

func loadBasketRoute(g *echo.Group) {
	memDb := db.NewMemDbImpl()
	prodDb := db.NewDbImpl()
	promDb := db.NewPromotionsDb()
	bs := service.NewBasketService(prodDb, memDb, promDb)
	bh := handler.NewBasketHandler(bs)
	g.POST("", bh.CreateBasket)
	g.POST("/:basketId/product", bh.AddProduct)
	g.DELETE("/:basketId/products/:productId", bh.RemoveProduct)
	g.GET("/:basketId", bh.GetAllProducts)
	g.GET("/:basketId/total", bh.GetTotal)
}
