package handler

import (
	"github.com/labstack/echo"
	"net/http"
)

type HealthHandler struct{}

func (hh *HealthHandler) Health(c echo.Context) error {
	return c.NoContent(http.StatusOK)
}
