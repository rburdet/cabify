package handler

import (
	"github.com/labstack/echo"
	"gitlab.com/rburdet/cabify/service"
	"net/http"
)

type ProductHandler struct {
	service *service.ProductService
}

func NewProductHandler(s *service.ProductService) *ProductHandler {
	return &ProductHandler{s}
}

func (ph *ProductHandler) ListAllProducts(c echo.Context) error {
	return c.JSON(http.StatusOK, ph.service.ListAllProducts())
}
