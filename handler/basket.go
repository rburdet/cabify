package handler

import (
	"fmt"
	"github.com/labstack/echo"
	"gitlab.com/rburdet/cabify/service"
	"net/http"
)

type BasketHandler struct {
	service *service.BasketService
}

func NewBasketHandler(s *service.BasketService) *BasketHandler {
	return &BasketHandler{s}
}

func (bh *BasketHandler) CreateBasket(c echo.Context) error {
	response := bh.service.CreateBasket()
	return c.JSON(http.StatusOK, struct {
		ID string `json:"id"`
	}{response})
}

func (bh *BasketHandler) AddProduct(c echo.Context) error {
	m := make(map[string]string)
	if err := c.Bind(&m); err != nil {
		return err
	}
	prodID := m["id"]

	basketId := c.Param("basketId")

	bh.service.AddProduct(basketId, prodID)
	return c.NoContent(http.StatusOK)
}

func (bh *BasketHandler) RemoveProduct(c echo.Context) error {
	basketId := c.Param("basketId")
	prodId := c.Param("productId")

	bh.service.RemoveProduct(basketId, prodId)
	return c.NoContent(http.StatusOK)
}

func (bh *BasketHandler) GetAllProducts(c echo.Context) error {
	basketId := c.Param("basketId")
	response := bh.service.GetAllProducts(basketId)
	return c.JSON(http.StatusOK, response)
}

func (bh *BasketHandler) GetTotal(c echo.Context) error {
	basketId := c.Param("basketId")
	gross, total, savings := bh.service.GetTotalAmmount(basketId)
	grossStr := fmt.Sprintf("%.2f€", gross)
	totalStr := fmt.Sprintf("%.2f€", total)
	savingsStr := fmt.Sprintf("%.2f%%", savings)
	return c.JSON(http.StatusOK, struct {
		Gross   string `json:"gross"`
		Total   string `json:"total"`
		Savings string `json:"savings"`
	}{grossStr, totalStr, savingsStr})
}
