package service

import (
	"fmt"
	"github.com/rs/xid"
	"gitlab.com/rburdet/cabify/db"
	"gitlab.com/rburdet/cabify/model"
)

type BasketService struct {
	db     db.Db
	memDb  db.MemDb
	promDb db.PromotionDb
}

func NewBasketService(db db.Db, memDb db.MemDb, promDb db.PromotionDb) *BasketService {
	return &BasketService{db, memDb, promDb}
}

func (bs *BasketService) getBasketProduct(basketId string, prodId string) (*model.Basket, *model.Product) {
	basket := bs.memDb.GetBasket(basketId)
	product := bs.db.GetProduct(prodId)
	return basket, product
}

func (bs *BasketService) CreateBasket() string {
	id := xid.New().String()
	bs.memDb.SaveBasket(id, &model.Basket{})
	return id
}

func (bs *BasketService) AddProduct(basketId string, prodId string) {
	basket, product := bs.getBasketProduct(basketId, prodId)
	if basket != nil {
		basket.AddProduct(product)
	}
}

func (bs *BasketService) RemoveProduct(basketId string, prodId string) {
	basket, product := bs.getBasketProduct(basketId, prodId)
	if basket != nil {
		basket.RemoveProduct(product)
	}
}

func (bs *BasketService) GetAllProducts(basketId string) []*model.Product {
	basket := bs.memDb.GetBasket(basketId)
	if basket != nil {
		return basket.GetAllProducts()
	}
	return nil
}

func (bs *BasketService) GetTotalAmmount(basketId string) (float64, float64, float64) {
	basket := bs.memDb.GetBasket(basketId)
	if basket != nil {
		basket.RefreshDiscount()
		gross := basket.GetTotalAmmount()
		for _, discounter := range bs.promDb.GetStrategies() {
			fmt.Println("Apling ")
			discounter.CalculateAmmount(basket)
			fmt.Printf("Discount: %v\n", basket.GetDiscountAmmount())
		}
		fmt.Printf("Total: %v\n", basket.GetDiscountAmmount())
		total := basket.GetDiscountAmmount()
		savings := (gross - total) / gross * 100
		return gross, total, savings
	}
	return 0, 0, 0
}
