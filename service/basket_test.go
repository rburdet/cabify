package service

import (
	"gitlab.com/rburdet/cabify/db"
	"gitlab.com/rburdet/cabify/model"
	"testing"
)

var (
	basket = model.Basket{}
	p1     = model.Product{Code: "VOUCHER", Name: "Cabify Voucher", Price: 5.00}
)

func restore() {
	basket = model.Basket{}
}

type mockDb struct{}

func (db *mockDb) GetProduct(id string) *model.Product { return &p1 }
func (db *mockDb) GetAllProducts() []*model.Product    { return []*model.Product{} }

type mockMemDb struct{}

func (m *mockMemDb) SaveBasket(id string, b *model.Basket) {}
func (m *mockMemDb) GetBasket(id string) *model.Basket     { return &basket }
func (m *mockMemDb) DeleteBasket(id string)                {}

func TestCreateBasket(t *testing.T) {
	bs := NewBasketService(&mockDb{}, &mockMemDb{}, db.PromotionDb{})
	basketId1 := bs.CreateBasket()
	basketId2 := bs.CreateBasket()

	if basketId1 == basketId2 {
		t.Error("Basket IDS should be different but are equal")
	}
}

func TestAddProduct(t *testing.T) {
	defer restore()
	bs := NewBasketService(&mockDb{}, &mockMemDb{}, db.PromotionDb{})
	basketId := bs.CreateBasket()
	bs.AddProduct(basketId, p1.Code)

	products := bs.memDb.GetBasket(basketId).GetAllProducts()
	if len(products) != 1 {
		t.Errorf("Basket should have 1 product but has %v", len(products))
	}

	if products[0] != &p1 {
		t.Errorf("Got %v, expected: %v", products[0], p1)
	}
}

func TestRemoveProduct(t *testing.T) {
	defer restore()
	bs := NewBasketService(&mockDb{}, &mockMemDb{}, db.PromotionDb{})
	basketId := bs.CreateBasket()
	bs.AddProduct(basketId, p1.Code)
	bs.RemoveProduct(basketId, p1.Code)
	products := basket.GetAllProducts()

	if len(products) != 0 {
		t.Errorf("Basket should have 0 product but has %v", len(products))
	}
}

func TestGetAllProducts(t *testing.T) {
	defer restore()
	bs := NewBasketService(&mockDb{}, &mockMemDb{}, db.PromotionDb{})
	basketId := bs.CreateBasket()
	bs.AddProduct(basketId, p1.Code)
	bs.AddProduct(basketId, p1.Code)
	products := bs.GetAllProducts(basketId)

	if products[0] != &p1 {
		t.Errorf("Got %v, expected: %v", products[0], p1)
	}

	if products[1] != &p1 {
		t.Errorf("Got %v, expected: %v", products[1], p1)
	}
}
