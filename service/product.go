package service

import (
	"gitlab.com/rburdet/cabify/db"
	"gitlab.com/rburdet/cabify/model"
)

type ProductService struct {
	db db.Db
}

func NewProductService(db db.Db) *ProductService {
	return &ProductService{db}
}

func (ps *ProductService) ListAllProducts() []*model.Product {
	return ps.db.GetAllProducts()
}
